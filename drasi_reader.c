/* Drasi readout of ALPIDE detectors.
 * Developed by:
 **** Bastian Loeher: b.loeher@gsi.de
 **** Martin Bajzek:  m.bajzek@gsi.de
 **** Luke Rose:      l.rose@gsi.de
 * 
 * CHANGELOG
 * 07.08 added DOUBLE_TIMESTAMP_READOUT define, such that after WR+Sync comes 
 * another two words which are the result of systemtime poll (CLOCK_REALTIME) -- M.B.
 */

#define kRED "\x1B[31m"
#define kGRN "\x1B[32m"
#define kBLUE "\x1B[34m"
#define kNRM "\x1B[0m"

#include <cstdint>
bool is_bad_event = false;
volatile uint64_t retries = 0;

#include "TSetup.h"
#include "TDevice.h"
#include "TScanConfig.h"
#include "TDeviceOccupancyScan.h"
#include "TDeviceBuilderIBSingleMosaic.h"
#include "TReadoutBoard.h"
#include "TReadoutBoardMOSAIC.h"
#include "TBoardConfigMOSAIC.h"
#include "TChipConfig.h"
#include "TDeviceThresholdScan.h"
#include "AlpideDictionary.h"
#include "masking/masking.cpp"

extern "C" {
    #include <lwroc_message.h>
    #include <lwroc_mon_block.h>
    #include <lwroc_net_conn_monitor.h>
    #include <lwroc_readout.h>
    #include <lwroc_parse_util.h>
    #include <lwroc_data_pipe.h>
    #include <lmd/lwroc_lmd_event.h>
    #include <lmd/lwroc_lmd_white_rabbit_stamp.h>
    #include <lmd/lwroc_lmd_ev_sev.h>
    #include <lmd/lwroc_lmd_util.h>
}
#include <cassert>

#if 0
#define _DEBUG /* Keep this define for more verbose diagnostics. */
#endif

/* Compressed data structure or raw. */
#if 1
#define COMPRESSED
#endif

/* Keep this define if RTCLK isn't supplied or is unstable. To have 
 * some kind of rough timestamping with UNIX system time. */
//#define USE_CLOCK_GETTIME

/* Next define enables double timestamp readout: RTCLK + System time.
 * Main timestamp will come from RTCLK, but system time will be added as another two words in the data stream. */
#define DOUBLE_TIMESTAMP_READOUT

#if defined(DOUBLE_TIMESTAMP_READOUT)
    #undef USE_CLOCK_GETTIME 
#endif

using namespace std;

vector<int> alpidesOn;

typedef shared_ptr<TDevice> TDevice_ptr;
typedef shared_ptr<TScanConfig> TScanConfig_ptr;
typedef shared_ptr<TReadoutBoard> TReadoutBoard_ptr;
typedef shared_ptr<TBoardConfigMOSAIC> TBoardConfigMOSAIC_ptr;
typedef shared_ptr<TChipConfig> TChipConfig_ptr;
typedef shared_ptr<TReadoutBoardMOSAIC> TReadoutBoardMOSAIC_ptr;
typedef shared_ptr<TAlpide> TAlpide_ptr;

typedef unique_ptr<TSetup> TSetup_ptr;

#define BUFFER_SIZE (1 << 25) /* 32 MByte */
#define LENGTH(x) (sizeof x / sizeof *x)

void cmdline_usage(void);
void init(void);
int parse_cmdline_arg(const char *);
void read_event(uint64_t, uint16_t);
void pretty_print_data(void*, unsigned);
void readout_loop(int *);

extern struct lwroc_readout_functions _lwroc_readout_functions;
extern volatile int _lwroc_quit;
extern lwroc_thread_block *_main_thread_block;
extern lwroc_pipe_buffer_control *_lwroc_main_data;
extern lwroc_monitor_main_block  _lwroc_mon_main;
extern lwroc_mon_block          *_lwroc_mon_main_handle;
extern lwroc_net_conn_monitor   *_lwroc_mon_main_system_handle;

/* ALPIDE global handles. */

TSetup* s;
TDevice* d;
TReadoutBoardMOSAIC* myBoard;

bool enablePulse;
bool enableTrigger;
std::vector<string> fpgaToAlpideID;

int nBytes;
unsigned char tempBuffer[BUFFER_SIZE] = {0};

enum TriggerMode {
    TM_INTERNAL = 0,
    TM_EXTERNAL = 1,
    TM_CONTINUOUS = 2
};

static struct {
    unsigned ctrl;
    unsigned crate;
    unsigned proc;
    unsigned wr_id;
    unsigned ids;	
    unsigned ntrigs;
    unsigned ntrigs_send;
    unsigned ntrigs_readout;
    unsigned thrs;
    unsigned vcasn;
    unsigned region;
    unsigned int delta_v;
    enum TriggerMode trigger_mode;
    unsigned debug;
} g_config = {0};

static lmd_stream_handle *g_lmd_stream;
//Check ports numbers for multiple alpides?

void cmdline_usage()
{
    printf("  --crate=N               Mark subevents with crate N.\n");
    printf("  --ctrl=N                Mark subevents with ctrl N.\n");
    printf("  --proc=N                Mark subevents with proc N.\n");
    printf("  --wr=N                  Mark WR stamp with ID N.\n");
    printf("  --ids=N				  Id numbers of MOSAIC to read.\n");
    printf("  --ntrigs=N              Number of triggers per cycle.\n");
    printf("  --trigger-mode=N        Trigger mode to run in:\n");
    printf("                           N=0: internal MOSAIC trigger (default).\n");
    printf("                           N=1: external MOSAIC trigger.\n");
    printf("                           N=2: ALPIDE continuous trigger.\n");
    printf("  --debug=N                Show more info on terminal:\n");
    printf("                           N=0: on (default).\n");
    printf("                           N=1: off (not default).\n"); 
    printf("  --thrs=N                Show more info on terminal:\n");
    printf("                           N=0: off (default).\n");
    printf("                           N=1: on (not default).\n"); 
    printf("  --vcasn=N        vcasn:\n");
    printf("\n");
}

#include "./maps/template.C"
void init()
{
    /* Load struct. Remember to #include correct above! */
    map_function();

    g_lmd_stream = lwroc_get_lmd_stream("READOUT_PIPE");
    lwroc_init_timestamp_track(); 

    if (g_config.trigger_mode == TM_EXTERNAL) {
        enablePulse = false;
        enableTrigger = false; 
    } else {
        enablePulse = true;
        enableTrigger = true; 
    }

    printf("Loading setup for MOSAIC[%d]:\n",g_config.ids);

    TSetup* tempSetup = new TSetup();
    s = tempSetup;
   
    s->ReadConfigFile("./config/MASTER.cfg");

    // s->ReadSpecificConfigFile((string(CONFIG_PATH "/MOSAIC")+ to_string(g_config.ids) + string(".cfg")).c_str());//read here mosaic/chip specific params

    const char* address = (std::string(net_id) + std::to_string(200 + g_config.ids)).c_str();
	printf("address = %s\n", address);
    s->SetDeviceAddress(address); 

    printf("  [%d] Setting device address to '%s'\n",g_config.ids, address);

    s->InitializeSetup();
    d = s->GetDevice().get();
    std::cout << "There are: " << MOSAIC[g_config.ids].chipId.size() << " chips in config file" << std::endl;

    for(int chip = 0; chip<MOSAIC[g_config.ids].chipId.size(); ++chip) {
        //Shows which chip we will try make work 
        std::cout << "Init chip:" << MOSAIC[g_config.ids].chipId.at(chip) << std::endl;

        //This allows for config and dumps defaults
        d->GetChip(MOSAIC[g_config.ids].chipId.at(chip))->ActivateConfigMode();
        if(g_config.debug==0) {
            std::cout << "HERE ALL DEFAULT REG BEFORE CONFIG FILE CHANGES FOR CHIP:" << MOSAIC[g_config.ids].chipId.at(chip) << std::endl;  
            d->GetChip(MOSAIC[g_config.ids].chipId.at(chip))->DumpConfig();
        }

        //Now init chip and modifiy parameters 
        auto config = d->GetChipConfig(MOSAIC[g_config.ids].chipId.at(chip));
        config->SetChipId(MOSAIC[g_config.ids].chipId.at(chip));

        config->SetReceiver(MOSAIC[g_config.ids].recId.at(chip));

        config->SetParamValue("RECEIVER", MOSAIC[g_config.ids].recId.at(chip));
        config->SetParamValue("ITHR", MOSAIC[g_config.ids].chipITHR.at(chip));
        config->SetParamValue("VCASN", MOSAIC[g_config.ids].chipVCASN.at(chip));
        config->SetParamValue("VCASN2", MOSAIC[g_config.ids].chipVCASN2.at(chip));

        config->SetParamValue("PULSEDURATION",MOSAIC[g_config.ids].pulse_duration);
        
		if(g_config.thrs==1) {
			config->SetParamValue("VPULSEL",g_config.delta_v);
		}

        std::cout <<"SET CHIPID:"<<MOSAIC[g_config.ids].chipId.at(chip)<<" RECEIVER:" <<config->GetParamValue("RECEIVER")<<std::endl;			

        //Check registers of chip
        d->GetChip(MOSAIC[g_config.ids].chipId.at(chip))->BaseConfig();
        d->GetChip(MOSAIC[g_config.ids].chipId.at(chip))->DumpConfig();

        d->GetChip(MOSAIC[g_config.ids].chipId.at(chip))->ActivateReadoutMode();
        TAlpide* theAlpide = d->GetChip(MOSAIC[g_config.ids].chipId.at(chip)).get();  

        printf("Loading mask for ALPIDE chip with ID '%s':\n",MOSAIC[g_config.ids].chipName.at(chip));

        // Needs modifying for multi sensor device !!!
        
        if(g_config.thrs == 0) {
            ApplyMask(theAlpide, (string( "./masking/mask_")
                        + MOSAIC[g_config.ids].fpc_name
                        + string("_").c_str()
                        + to_string(MOSAIC[g_config.ids].chipId.at(chip))
                        + string(".cfg")).c_str());
            std::cout<< (string( "./masking/mask_")
                        + MOSAIC[g_config.ids].fpc_name
                        + string("_").c_str()
                        + to_string(MOSAIC[g_config.ids].chipId.at(chip))
                        + string(".cfg")).c_str()<<std::endl;
        }
	 
        if(g_config.thrs==1) {
           //May Take ages to mask all this so change sleeps according 
            ApplyMask(theAlpide,(string("./masking/oddRows.cfg")).c_str());
            theAlpide->WritePixRegAll(AlpidePixConfigReg::PULSE_ENABLE,true);
            
        }

        //Here are mosaic globals.
        TReadoutBoard* testBoard = d->GetBoard(0).get();
        myBoard = dynamic_cast<TReadoutBoardMOSAIC*>(testBoard);

        //myBoard->SetVerboseLevel(4); 
        myBoard->setSpeedMode(MosaicReceiverSpeed::RCV_RATE_400); // 400(default),600,1200 -- Martin B
       
        //d->GetBoardConfig(0)->SetParamValue("DEVICENAME",MOSAIC_GLOBAL.device_name);

        d->GetBoardConfig(0)->SetParamValue("STROBEDELAYBOARD",MOSAIC_GLOBAL.strobe_delay_board);
        d->GetBoardConfig(0)->SetParamValue("PULSEDELAY",MOSAIC_GLOBAL.pulse_delay);
        //d->GetBoardConfig(0)->SetParamValue("PULSEDELAYBOARD",MOSAIC_GLOBAL.pulse_delay_board);
        //d->GetBoardConfig(0)->SetParamValue("STROBEDURATION",MOSAIC_GLOBAL.strobe_duration);
        //d->GetBoardConfig(0)->SetParamValue("STROBEDELAYCHIP",MOSAIC_GLOBAL.strobe_delay_chip);
        //d->GetBoardConfig(0)->SetParamValue("TRIGDELAY",MOSAIC_GLOBAL.trig_delay);
        d->GetBoardConfig(0)->SetParamValue("CONTROLLATENCYMODE",MOSAIC_GLOBAL.ctrl_latency_mode);
        d->GetBoardConfig(0)->SetParamValue("POLLINGDATATIMEOUT",MOSAIC_GLOBAL.polling_data_time_out);
        d->GetBoardConfig(0)->SetParamValue("DATALINKPOLARITY",MOSAIC_GLOBAL.data_link_polarity); 
        d->GetBoardConfig(0)->SetParamValue("DATALINKSPEED",MOSAIC_GLOBAL.data_link_speed);
        d->GetBoardConfig(0)->SetParamValue("TRGRECORDERENABLE",MOSAIC_GLOBAL.trg_record_enable);

        //Luke suggestons 
        d->GetBoardConfig(0)->SetParamValue("MASTERSLAVEMODEON",MOSAIC_GLOBAL.master_slave_mode_on);

        //After set from struct
        const int triggerDelay =
            d->GetBoardConfig(0)->GetParamValue("STROBEDELAYBOARD"); 

        /* note: this used to be multiplied by 10 */
        const int pulseDelay =
            d->GetBoardConfig(0)->GetParamValue("PULSEDELAY");

        /* Here is where we would change for internal or external trigger. */
        myBoard->SetTriggerConfig(enablePulse, enableTrigger, triggerDelay, pulseDelay);
        if (g_config.trigger_mode == TM_EXTERNAL) {
            myBoard->SetTriggerSource(TTriggerSource::kTRIG_EXT);
        } else {
            myBoard->SetTriggerSource(TTriggerSource::kTRIG_INT);
        }

        myBoard->DumpConfig(); 	
        tempSetup = nullptr;

        switch (g_config.trigger_mode) {
            case TM_INTERNAL:
                printf("############# INTERNAL_TRIGGER ENABLED #############\n");
                printf("  will send %d software triggers to MOSAIC\n", g_config.ntrigs);
                printf("  will then use same value for readout\n");
                g_config.ntrigs_send = g_config.ntrigs;
                g_config.ntrigs_readout = g_config.ntrigs;
                break;
            case TM_EXTERNAL:
                printf("############# EXTERNAL_TRIGGER ENABLED #############\n");
                printf("  will not send software triggers to MOSAIC\n");
                g_config.ntrigs_send = 0;
                if (g_config.ntrigs == 0) {
                    g_config.ntrigs_readout = 1000;
                } else {
                    g_config.ntrigs_readout = g_config.ntrigs;
                }
                printf("  will set ntrigs to %d for readout\n",
                        g_config.ntrigs_readout);
                break;
            case TM_CONTINUOUS:
                printf("############# CONTINUOUS_TEST ENABLED #############\n");
                printf("  will send 1 software trigger to MOSAIC\n");
                printf("  will set ntrigs to 10000 for readout\n");
                g_config.ntrigs_send = 1;
                g_config.ntrigs_readout = 10000;
                break;
        }
        
        myBoard->StartRun();
        std::cout << "End of init() function ...\n\n";

    }
}

void lwroc_readout_setup_functions() {}

void lwroc_readout_pre_parse_functions()
{
    memset(&g_config, 0, sizeof g_config);
    g_config.ntrigs = 1;
    
    _lwroc_readout_functions.fmt = &_lwroc_lmd_format_functions; // added 03.10.2023 --M.B
    _lwroc_readout_functions.init = init;
    _lwroc_readout_functions.read_event = read_event; 
    _lwroc_readout_functions.untriggered_loop = readout_loop; 
    _lwroc_readout_functions.cmdline_fcns.usage = cmdline_usage;
    _lwroc_readout_functions.cmdline_fcns.parse_arg = parse_cmdline_arg;
}

int parse_cmdline_arg(char const *request)
{
	char const *post;

    /* TODO: Right now order of arguments matters. It should NOT. */

    if (LWROC_MATCH_C_PREFIX("--crate=", post)) {
        g_config.crate = atol(post);
        cout << "Parsed --crate: " << g_config.crate << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--ctrl=", post)) {
        g_config.ctrl = atol(post);
        cout << "Parsed --ctrl: " << g_config.ctrl << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--proc=", post)) {
        g_config.proc = atol(post);
        cout << "Parsed --proc: " << g_config.proc << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--wr=", post)) {
        g_config.wr_id = atol(post);
        cout << "Parsed --wr: " << g_config.wr_id << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--ids=", post)) {
        g_config.ids = atol(post);
        cout << "Parsed --ids: " << g_config.ids << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--vcasn=", post)) {
        g_config.vcasn = atol(post);
        cout << "Parsed --vcasn: " << g_config.vcasn << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--region=", post)) {
        g_config.region = atol(post);
        cout << "Parsed --region: " << g_config.region <<endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--ntrigs=", post)) {
        g_config.ntrigs = atol(post);
        cout << "Parsed --ntrigs: " << g_config.ntrigs <<  endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--trigger-mode=", post)) {
        g_config.trigger_mode = (enum TriggerMode)atol(post);
        cout << "Parsed --trigger-mode: " << g_config.trigger_mode << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--debug=", post)) {
        g_config.debug = atol(post);
        cout << "Parsed --debug: " << g_config.debug << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--thrs=", post)) {
        g_config.thrs = atol(post);
        cout << "Parsed --thrs: " << g_config.thrs << endl;
    } 
    else if (LWROC_MATCH_C_PREFIX("--delta_v=", post)) {
        g_config.delta_v = atol(post);
        cout << "Parsed --delta_v: " << g_config.delta_v << endl;
    } 
    else {
        return 0;
    }
    return 1;
}

void check_data(uint8_t *buffer, size_t size)
{
    uint8_t *p = buffer;
    uint8_t *end = buffer + size;

    printf("check_data, size = %lu\n", size);

#define CHIP_HEADER_MARK  0xa0
#define CHIP_HEADER_MASK  0xf0
#define CHIP_TRAILER_MARK 0xb0
#define CHIP_TRAILER_MASK 0xf0
#define CHIP_EMPTY_MARK   0xe0
#define CHIP_EMPTY_MASK   0xfe
#define REGION_MARK       0xc0
#define REGION_MASK       0xe0
#define DATA_SHORT_MARK   0x40
#define DATA_SHORT_MASK   0xc0
#define DATA_LONG_MARK    0x00
#define DATA_LONG_MASK    0xc0

    while (p < end) {
        if ((*p & CHIP_HEADER_MASK) == CHIP_HEADER_MARK) {
            printf("%02x: chip_header, payload: %02x\n", *p, *(p+1));
            p += 2;
        } else if ((*p & CHIP_TRAILER_MASK) == CHIP_TRAILER_MARK) {
            printf("%02x: chip_trailer\n", *p);
            p += 1;
        } else if ((*p & CHIP_EMPTY_MASK) == CHIP_EMPTY_MARK) {
            printf("%02x: chip_empty, payload: %02x\n", *p, *(p+1));
            p += 2;
        } else if ((*p & REGION_MASK) == REGION_MARK) {
            printf("%02x: region\n", *p);
            p += 1;
        } else if ((*p & DATA_SHORT_MASK) == DATA_SHORT_MARK) {
            printf("%02x: data_short, payload: %02x\n", *p, *(p+1));
            p += 2;
        } else if ((*p & DATA_LONG_MASK) == DATA_LONG_MARK) {
            printf("%02x: data_long, payload: %02x %02x\n", *p, *(p+1), *(p+2));
            p += 3;
        } else {
            printf("%02x: unknown data item (next: %02x %02x %02x)\n",
                    *p, *(p+1), *(p+2), *(p+3));
            abort();
        }
    }
}

/* Supply a ptr to the 5-word buffer, and a 64-bt ts
 * Fcn returns a ptr to the next available data word. -- M.B. */
uint32_t* write_wr_stamp(uint32_t *p32, uint64_t ts) {
    uint64_t stamp;
    int bad_mark = 0;
    int rc = 0;

    /* use timestamp from MOSAIC data */
    stamp = ts; 

    /* RTCLK timestamp offset compensation. Correct for this for EVERY new experiment! */
    stamp += 154480;

    /* TS is obviously wrong when it is too large */
    if ((stamp >> 48) >= 0x3000) {
        bad_mark = WHITE_RABBIT_STAMP_EBID_ERROR;
    }

    rc = lwroc_report_event_timestamp(stamp, LWROC_TRACK_TIMESTAMP_FRESH);
    //rc = lwroc_report_event_timestamp(stamp, 0);

    if ((rc & LWROC_TRACK_TIMEST_RET_SEEMS_OK == 0)
     && (rc & LWROC_TRACK_TIMEST_RET_NO_FIT == 0)) {
		printf("Bad WR subevent!! RC = %lld\n", (int64_t)rc);
		printf("Current WR = 0x%x\n", ts);
        bad_mark = WHITE_RABBIT_STAMP_EBID_ERROR;
    }

    *p32++ = g_config.wr_id << 8  |
        (bad_mark ? WHITE_RABBIT_STAMP_EBID_ERROR : 0);
    *p32++ = WHITE_RABBIT_STAMP_LL16_ID |
        (uint32_t)((stamp >>  0) & 0xffff);
    *p32++ = WHITE_RABBIT_STAMP_LH16_ID |
        (uint32_t)((stamp >> 16) & 0xffff);
    *p32++ = WHITE_RABBIT_STAMP_HL16_ID |
        (uint32_t)((stamp >> 32) & 0xffff);
    *p32++ = WHITE_RABBIT_STAMP_HH16_ID |
        (uint32_t)((stamp >> 48) & 0xffff);
	return p32;
}

void temp_timer(ofstream &temp_file)
{
    
    //Proof of concept -> Use WR --lrose 14/07/2024
    //Need DT from MOSAIC for it to work properly !! 
    //Or a method to pause the readoutloop
    const auto p1 = std::chrono::system_clock::now();
  
    //Ten MOSAIC add to list\\
    //Might have to calibrate, check the ALICE code

    //temp_file.open("temp_file.txt",std::ios_base::in);
    if(temp_file.is_open()){
    for(int nM = 0 ; nM<10;nM++){
        for(int chip = 0 ; chip < MOSAIC[nM].chipId.size();chip++) {
                TAlpide*theAlpide = d->GetChip(MOSAIC[nM].chipId.at(chip)).get();  
                temp_file<<std::chrono::duration_cast<std::chrono::nanoseconds>(p1.time_since_epoch()).count()<<" "<<nM<<" "<<MOSAIC[nM].chipId.at(chip)<<" "<<theAlpide->ReadTemperature()<<" \n";
                std::cout<<std::chrono::duration_cast<std::chrono::nanoseconds>(p1.time_since_epoch()).count()<<" "<<nM<<" "<<MOSAIC[nM].chipId.at(chip)<<" "<<theAlpide->ReadTemperature()<<" \n";
      
               
            }
        }
    }
std::cout<<std::endl;
//temp_file.close();

}
void read_event(uint64_t cycle, uint16_t trig)
{

#if defined(USE_CLOCK_GETTIME) | defined(DOUBLE_TIMESTAMP_READOUT)
    static struct timespec tnow;
#define _LEAP_SECONDS 28.02
    clock_gettime(CLOCK_REALTIME, &tnow);
    uint64_t system_time = (1000000000 * (uint64_t)tnow.tv_sec +
            (uint64_t)tnow.tv_nsec) +
            (uint64_t)(_LEAP_SECONDS * 1000000000);
#endif

    static uint64_t event=0;
	is_bad_event = false;

    struct lwroc_lmd_subevent_info info;
    lmd_event_10_1_host *ev;
    lmd_subevent_10_1_host *sev;
    uint32_t *p32;
    uint8_t *p8;
    size_t event_size = sizeof (lmd_subevent_10_1_host) + BUFFER_SIZE;
    uint32_t buf_bytes;
    int i;

    lwroc_reserve_event_buffer(g_lmd_stream, cycle, event_size, 0, 0);

    lwroc_new_event(g_lmd_stream, &ev, trig);
   
    if (g_config.trigger_mode != TM_EXTERNAL) {
        myBoard->Trigger(g_config.ntrigs_send);
        /*--Double trigger the board internal 
        usleep(2.5);  
        myBoard->Trigger(g_config.ntrigs_send);
        */
    }

#ifdef _DEBUG
    printf("#### start readout of event=%llu\n", event);
#endif
   
    event++;

    info.type = 10;
    info.subtype = 1;
    info.control = g_config.ctrl;
    info.subcrate = g_config.crate;
    info.procid = g_config.proc;

    p32 = (uint32_t *)lwroc_new_subevent(g_lmd_stream,
            LWROC_LMD_SEV_NORMAL, &sev, &info);      

	uint32_t* p32_begin = p32;

    int n = 0;
	for (n = 0; n < g_config.ntrigs_readout; ++n) {
#ifdef _DEBUG
		printf("ntrigs_readout: n=%d of %d\n", n, g_config.ntrigs_readout);
		printf("reading trigger data\n");
#endif

retry:
		int readDataFlag = myBoard->ReadEventData(nBytes, tempBuffer);

		/* For the first n=0 call, poll the trigger data. 
		 * Extract timestamp, to form the first five words. */
		if (n == 0) {
			if(readDataFlag != MosaicDict::kTRGRECORDER_EVENT) {
				cerr << endl << __PRETTY_FUNCTION__ << " : wrong data order. Expected trigger data. Aborting\n";
				cerr << "Got data flag: " << readDataFlag << endl;
#ifdef _DEBUG
				if(readDataFlag == -1) {
					printf("    -- Got empty event flag.. , will try again.");
					printf("    -- Managed to poll %d bytes.", nBytes);
					printf("Found the following sequence of words:");
					for(int ii=0; ii<nBytes; ++ii) {
						printf("%x%x",(int)(tempBuffer[ii]>>4), (int)(tempBuffer[ii] & 0xf));
						if(ii%4 == 0) printf(" ");
						if(ii%16 == 0) printf("\n");
					}
					printf("\n");
				}
				cout << std::flush;
#endif
				/* sleep(1); //MB Edit 14.07.23 */
				/* usleep(50); */
				//abort();

				usleep(50);
				is_bad_event = true;
				goto retry;
			}
			assert((nBytes > 18*sizeof(uint32_t) && "nBytes > 18 words failed."));

			uint64_t hi, mi, lo, ts;
			uint32_t *p = (uint32_t*)&tempBuffer[0];
			hi = p[19] & 0xffff;
			//printf("hi = p[19] & 0xffff; p[19] = 0x%08x\n", p[19]);
			mi = p[18];
			//printf("mi = p[18]; p[18] = 0x%08x\n", p[18]);
			lo = p[17] >> 16;
			//printf("lo = p[17] >> 16; p[17] = 0x%08x\n", p[17]);
			ts = (hi << 48) | (mi << 16) | lo;

            p32 = write_wr_stamp(p32, 
#ifdef USE_CLOCK_GETTIME
                    system_time             
#else
			        ts
#endif
            );

			uint16_t trig_info = p[17] & 0xffff;
			uint8_t trigger_type = trig_info & 0x3;
			uint8_t trig_marker = (trig_info >> 2) & 0x1;
			uint16_t trigger_length = trig_info >> 3;
			uint32_t sync_mark = 0;
			if (trig_marker != 0) {
				printf("ERROR: Expected trig_marker=0x0, but got: %02x\n",
						trig_marker);
				is_bad_event = true;
			}
			if (trigger_type > 2) {
				printf("ERROR: Expected trigger_type=0,1,2 but got: %u\n",
						trigger_type);
				is_bad_event = true;
			}
			/* Set event header trigger info accordingly. */
			ev->_info.i_trigger = (trigger_type == 2) ? 3 : 1;

			/* Sync check. */
			sync_mark = SYNC_CHECK_RECV | trigger_length;
			*p32++ = SYNC_CHECK_MAGIC | sync_mark;

#ifdef DOUBLE_TIMESTAMP_READOUT
			*p32++ = (uint32_t)system_time;
			*p32++ = *((uint32_t*)&system_time + 1);
#endif
#ifdef _DEBUG
			printf("   Data flag (from trig): %d\n", readDataFlag);
			printf("   nBytes (from trig): %d \n", nBytes);
			pretty_print_data(tempBuffer, nBytes);
#endif 
		}

#if defined(COMPRESSED)
		/* Copy over the first 4 words of the buffer, they `might` be useful. */
		//memcpy(p32, tempBuffer, 4 * sizeof(uint32_t));
		//p32 += 4;

		uint32_t* buffer_ptr = (uint32_t*) tempBuffer;

		/* Skip initial 4 words. Plus next 12 words, they are all zeroes, related to bunch crossing. */
		buffer_ptr += 4 + 12;
		nBytes -= 16*sizeof(uint32_t);

		/* Remaining buffer contains only Whiterabbit stuff. Got copied over in the `write_wr_stamp` call. */

#else /* Not compressed. Copy over the whole thing. */
		memcpy(p32, tempBuffer, nBytes);
		p32 += (nBytes+3)/4;
#endif


#if defined(COMPRESSED)
		/* Add additional word at the end of ALPIDE_TRIG part */ 
		/* to identify how many chips are read (and not dropped) */
		uint32_t* chip_count_w = p32;
		*p32++ = 0xdeadbee << 4;
		int fired_chips = 0;
#endif


#ifdef _DEBUG
		printf("Reading out %d chips\n", MOSAIC[g_config.ids].chipId.size());
#endif
        /* Loop now over ALPIDE chips on the board. Each chip requires its own ReadEventData() call. */
		for(int al = 0; al<MOSAIC[g_config.ids].chipId.size(); ++al) {
#ifdef _DEBUG
			printf("  alpide chip (al) = %d\n", al);
#endif

			readDataFlag = myBoard->ReadEventData(nBytes, tempBuffer);
#ifdef _DEBUG
			printf("Managed to poll %d bytes from chip\n", nBytes);
#endif

			/* Data supression need to speed this loop up it slows down mosaics too much by factor n for number of chips :( --Luke */
#ifdef COMPRESSED
			if(readDataFlag==2 && nBytes==66) {
				continue;
			} else {
				fired_chips++;
			}
#endif

			if(readDataFlag == 0) {
				cerr << __PRETTY_FUNCTION__ << " : no data.\n";
				abort();
				is_bad_event = true;
				/* sleep(1); //MB-EDIT 14.07.23 */
			}
			else if(readDataFlag == MosaicDict::kTRGRECORDER_EVENT) {
				cerr << __PRETTY_FUNCTION__ << " : Received trigger data.\n";
				cerr << __PRETTY_FUNCTION__ << " : Expected chip data. Event marked as bad.\n";
				cerr << __PRETTY_FUNCTION__ << " : Alpide : " << MOSAIC[g_config.ids].chipId.at(al) << endl;
				is_bad_event = true;
				/* sleep(1); //MB-EDIT 14.07.23 */
			}
			else if(readDataFlag == MosaicDict::kEMPTY_EVENT) {
				cerr << __PRETTY_FUNCTION__ << " : no data when polling for chip data. Event marked as bad.\n";
				cerr << "Alpide : " <<  MOSAIC[g_config.ids].chipId.at(al)  << endl;
				is_bad_event = true;
				/* sleep(1); //MB-EDIT 14.07.23 */
			}
#ifdef _DEBUG
			else {
				cout << "    GREAT SUCCESS ~~ \n";
				cout << "    Alpide = " <<  MOSAIC[g_config.ids].chipId.at(al)  << endl;
				cout << "    Flag = " << readDataFlag << endl << endl;
				/* sleep(1); //MB-EDIT 14.07.23 */
			}
#endif

			/* Somehow  there is always 0x00 at tempBuffer[nBytes-1]
			 * Set to 0xff to not get confused during unpacking. */
			tempBuffer[nBytes - 1] = 0xff;

			/* Add padding to next 32 bit boundary */
			while ((nBytes & 0x3) != 0) {
				tempBuffer[nBytes++] = 0xff;
			}

			/* Byteswap data */
			{
				uint32_t* p = (uint32_t *)&tempBuffer[64];
				for (int i = 64; i < nBytes; i+=4) {
					uint32_t val =
						((*p & 0x000000ff) << 24)
						| ((*p & 0x0000ff00) <<  8)
						| ((*p & 0x00ff0000) >>  8)
						| ((*p & 0xff000000) >> 24);
					*p = val;
					p++;
				}
			}
#ifdef _DEBUG
			/* Check data. */
			check_data(&tempBuffer[64], nBytes - 64);
#endif
			/* Store the size with a 0xbb indicator on far left. */
			*p32++ = ((uint32_t)0xbb << 24) | nBytes;

			/* Store data */
#ifdef COMPRESSED
			// Copy over the first 4 words, they are useful
			memcpy(p32, tempBuffer, 4 * sizeof(uint32_t));
			p32 += 4;

			buffer_ptr = (uint32_t*) tempBuffer;

			// Skip next 12 words, they are all zeroes
			// They are related to bunch crossing
			buffer_ptr += 4 + 12;
			nBytes -= 16*sizeof(uint32_t);

			// Memcpy the remaining buffer:
			memcpy(p32, buffer_ptr, nBytes);
			p32 += nBytes / 4;
#else
			memcpy(p32, tempBuffer, nBytes);
			p32 += nBytes / 4;
#endif

		} /* End of chip loop. */

#ifdef COMPRESSED
		*chip_count_w |= fired_chips; /* 0 <= fired_chips <= 6 */
#endif

#ifdef _DEBUG
		printf("Done reading out %d chips\n", MOSAIC[g_config.ids].chipId.size());
#endif  
     
	} /* End of n_trigs readout loop. */

#ifdef _DEBUG
    printf("Readout done\n");
#endif

	if(is_bad_event) {
		retries++;
		if(retries > 10000) {
			printf("%sFound 10000 bad events in a row. Aborting at last!%s\n", kRED, kNRM);
			usleep(100);
			abort();
		}
	}
	else {
		if(retries > 0) {
			printf("%sFound good event finally after %llu retries.\n\n%s", kGRN, retries, kNRM);
		}
		retries = 0;
	}

    lwroc_finalise_subevent(g_lmd_stream, LWROC_LMD_SEV_NORMAL, p32);
    lwroc_finalise_event_buffer(g_lmd_stream);
}

void readout_loop(int *start_no_stop)
{
    uint64_t cycle;

    //ofstream temp_file("temp_file.txt");   
    //temp_file.open("temp_file.txt",std::ios_base::in);

    *start_no_stop = 0;
    for (cycle = 0; !_lwroc_main_thread->_terminate; ++cycle) {
   

    //Only way this will work is on and offspill trigger length meeasurment :(
     //if((int)cycle%1000==0){
        //std::cout<< cycle <<endl;
        //temp_timer(temp_file);
    //}

#ifdef _DEBUG
        printf("Starting cycle: %llu\n", cycle);
#endif
        read_event(cycle, 1);   
        LWROC_MON_CHECK_COPY_BLOCK(_lwroc_mon_main_handle,
                &_lwroc_mon_main, 0);
        LWROC_MON_CHECK_COPY_CONN_MON_BLOCK(
                _lwroc_mon_main_system_handle, 0);
    }
}

void pretty_print_data(void* pdata, unsigned nBytes) {
	int n=0;
	uint32_t* p32 = (uint32_t*)pdata;
	for(int i=0; i<nBytes / 4; ++i) {
		printf("%08x ", *p32++);
		if(i % 8 == 7) printf("\n");
	}
}





