# ALPIDE DAQ for FAIR-Phase 0

## Getting started
To successfully run ALPIDE's at GSI/FAIR, in a NUSTAR drasi-based configuration, software requirements are:
- ROOT 6.26+
- [Drasi](https://fy.chalmers.se/~f96hajo/drasi/doc/)
- [Alpide Software](https://git.gsi.de/m.bajzek/alpide-software)

Together with a superuser Linux PC. 
After configuring a local MOSAIC subnet as instructed in
Build both Drasi and Alpide Software utilities.

``git clone https://git.gsi.de/m.bajzek/alpide-daq``

After building *drasi* and *Alpide Software* somewhere, specifify their paths in the `Makefile.local` file. Path can be relative.

### Rules in the DAQ process reader
- MOSAIC configuration is governed by
	- Master file `config/MASTER.cfg`
	- Map file `./maps/map_XXX.C` file.
- MOSAIC's running ALPIDE's are to be on their own subnet, specified in the `maps/map_XXX.C` file as the `const char* net_id` string.
Master file shouldn't be edited, while the `map_XXX.C` file contains ALPIDE specifications such as threshold parameters and chip configuration per MOSAIC board.
The specification of which map file is used is found in the user function source code `drasi_reader.c`. Just above `void init()` function implementation.


General ALPIDE DAQ configuration is specified in the `Alpide.conf` file. Used for running multiple ALPIDE systems with a local timeorderer. 
Also for single DAQ nodes, the file specifies server ports. `ALPIDE_ARRAY` variable defines which MOSAIC boards are bundled together into a timeorderer/logger scheme.

## Useful extra utilities
For analysis purposes, a [UCESB](https://fy.chalmers.se/~f96hajo/ucesb/) based unpacker is employed. This serves to convert stored .lmd files into ROOT format.
- Install UCESB
- In the same directory as UCESB, clone the minimal [UPEXPS](https://git.gsi.de/m.bajzek/upexps/-/tree/mini)
- `cd upexps/`
- Clone the [ALPIDE specific unpacker](https://git.gsi.de/m.bajzek/alpide-unpacker)
- `ln -s alpide_unpacker alpide`
- Follow instructions inside `alpide/` directory
