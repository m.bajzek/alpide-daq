#!/bin/bash

source alpide.conf
set -x
if [ $# -lt 1 ] ; then
    echo "Usage ./run.bash id [to]"
    echo "  id is the MOSAIC id. "
    echo "  - id is used as the White Rabbit Subsystem ID."
    echo "  - id is used as the LMD subevent control number."
    echo "  - From the id other values are derived."
    echo "      -> port = id + 7000."
    echo "      -> stream port = id + 6100"
    echo "      -> trans port = id + 6200"
    echo " to: using the 'to' flag will try running with a (localhost) timeorder."
    exit
fi

id=$1
shift

use_to=$1
shift

if [ "x$use_to" == "xto" ] ; then
    opt="--server=drasi,dest=$TIMEORDER"
else
    opt="--log-no-start-wait"
fi

ip=$(bc <<< "200 + $id")
port=$(bc <<< "7000 + $id")
stream=$(bc <<< "6100 + $id")
trans=$(bc <<< "6200 + $id")
wr_id=$(((id)+45))

echo "IP = 192.168.XXX.$ip"
echo "port = $port"
echo "stream = $stream"
echo "trans = $trans"
echo "opt = $opt"

# Run in a loop to avoid downtime due to TCP errors
while true;
do

#1 is external, 0 internal. Look in drasi.reader.#1 is external, 0 internal. Look in drasi.reader.cc

#valgrind --tool=memcheck \
#./build_*/m_read_meb_cc --crate=${id} --ctrl=1 --proc=69 --wr=${wr_id} \
#gdb --args ./build_*/m_read_meb_cc --crate=0 --ctrl=${id} --proc=69 --wr=${wr_id} \
./build_$(gcc -dumpmachine)_$(gcc -dumpversion)/m_read_meb_cc --crate=0 --ctrl=$id --proc=69 --wr=${wr_id} \
    --port=$port \
    --label=ALPI${id} \
    --buf=size=500Mi \
    --server=stream:${stream},bufsize=100Mi,flush=1s \
    --ids=$id \
    --trigger-mode=1 \
	--ntrigs=1 \
    --max-ev-size=1Mi \
    --max-ev-interval=35s \
    --ntp=r3bntp3.gsi.de \
	--debug=1 \
    $opt "$@"	
    
sleep 5

done
