#!/bin/bash
OUTFILE=../rootfiles
UPEXPS=../../upexps

mkdir -p $OUTFILE
$UPEXPS/alpide/alpidedrasi $1 --ntuple=RAW,$(echo $1 | sed 's,lmd/,rootfiles/,;s,\.lmd,.root,') --debug --allow-errors
