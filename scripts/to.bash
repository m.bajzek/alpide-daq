#!/bin/bash

source Makefile.local
source alpide.conf

hosts=""
hosts+=" --drasi=ts-disable=45s,r4l-71:6003"
for i in ${ALPIDE_ARRAY[@]}
do
    echo "Fetching host: $i"
    num=$(printf "%03d" $i)
    hosts+=" --drasi=ts-disable=45s,localhost:7$num"
done

$DRASI_DIR/bin/lwrocmerge \
    --port=$TO_PORT \
    --server=trans:$TO_TRANS_PORT,flush=1,nohold \
    --server=stream:$TO_STREAM_PORT,flush=1 \
    --file-writer \
    --buf=size=4Gi \
    --merge-mode=wr \
    --max-ev-size=10Mi \
    --merge-ts-nodata-warn=30s \
    --merge-ts-analyse-ref=1 \
    --max-ev-interval=70 \
    --label=TO_ALPIDE \
    --ntp=r3bntp3.gsi.de \
    $hosts "$@"
#exit
#--server=drasi,dest=lxlanddaq01,flush=1 \
    
