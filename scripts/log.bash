#!/bin/bash
source ../Makefile.local
source alpide.conf

hosts=""
for l in ${ALPIDE_ARRAY[@]}
do
    num=$(printf "%03d" $l)
    hosts+=" localhost:7$num"
done

${DRASI_DIR}/bin/lwrocmon --log $TIMEORDER $hosts
