#!/bin/bash
#Give Me Timestamps in ns



source ../alpide.conf
time_stamp_local_ns=$(date +%s%N)
time_stamp_local=$(printf "0x%x" $time_stamp_local_ns)
#echo $time_stamp_local_ns
#echo $time_stamp_local

time_stamp_alpide=$(stdbuf --output=L ../ucesb/empty/empty --stream=localhost:$TO_STREAM_PORT \
    --max-events=1 --tstamp-print \
    2>/dev/null | stdbuf --output=L awk 'NR==2{print $4}') 

#echo $time_stamp_alpide
#takes some time to do so allow some latency!
time_stamp_alpide_hex=$(echo "$time_stamp_alpide"| sed "s/://g")

time_stamp_alpide_ns=$((16#$time_stamp_alpide_hex))
#echo $time_stamp_alpide_ns

time_diff_ns="$((time_stamp_local_ns-time_stamp_alpide_ns))"
#echo $time_diff_ns
time_diff_s="$((time_diff_ns/1000000000))"
echo $time_diff_s





