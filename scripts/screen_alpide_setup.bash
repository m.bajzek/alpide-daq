#!/bin/bash
. alpide.conf

screen -ls | grep -q alpide
if [ 0 -eq $? ]
then
    echo "Screen session running , kill with 'screen -S alpide -X quit'"
    exit 1 
fi

#create windows
screen -dmS alpide
screen -S alpide -p 0 -X title rate
screen -S alpide -X screen -t tree
screen -S alpide -X screen -t log
screen -S alpide -X screen -t to
screen -S alpide -X screen -t misc
screen -S alpide -X screen -t st

for i in ${ALPIDE_ARRAY[@]}
do
    moasic_id=$(echo $i | cut -d':' -f1)
    screen -S alpide -X screen -t run$i
done
screen -S alpide -p rate -X stuff "$DRASI_DIR/bin/lwrocmon --rate localhost:$TO_PORT"
screen -S alpide -p tree -X stuff "$DRASI_DIR/bin/lwrocmon --tree localhost:$TO_PORT"
