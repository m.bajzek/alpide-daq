#!/bin/bash
#In array has working MOSAIC and port base maps
. alpide.conf

for i in ${ALPIDE_ARRAY[@]}
do
    mosaic_id=$(echo $i | cut -d':' -f1)
    screen -S alpide run$mosaic_id -X stuff "^M./run.bash $mosaic_id to^M"
done

