#!/bin/bash
[[ $# < 1 ]] && { echo "Supply file name."; exit 1; }

OUTDIR=../lmd
mkdir -p $OUTDIR
../../ucesb/empty/empty --stream=localhost:8100 --output=$OUTDIR/$1
