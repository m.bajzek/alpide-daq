#!/bin/bash

# Run the time stitcher

source ./alpide.conf

coincidence_ns=2000

while true
do
    $UCESB_DIR/empty/empty \
        trans://localhost:$TO_TRANS_PORT \
        --time-stitch=wr,$coincidence_ns \
        --server=stream:$STITCH_PORT,flush=1
    sleep 2
done
