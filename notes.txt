[[ 20.08.2024 ]]
More info on:
https://wiki.r3b-nustar.de/detectors/alpide/

[[ 26.09.2024 ]]
EXPLODER RTCLK ECL output doesn't work.
MOSAIC loses PLL after 1-2min or so and starts drifting (either counting 1s too fast or too slow).

Solution: take LEMO output, feed to NACON to clean it up. Then take into ALPIDE via expensive LEMO's
