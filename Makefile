BUILD_DIR:=build_$(shell gcc -dumpmachine)_$(shell gcc -dumpversion)

TARGET:=$(BUILD_DIR)/m_read_meb_cc
SRC:=$(wildcard *.c)
OBJ:=$(patsubst %.c,$(BUILD_DIR)/%.o,$(SRC))

SCAN_TARGET:=scan_chips
SCAN_SRC:=scan_chips.cc
SCAN_OBJ:=$(BUILD_DIR)/scan_chips.oxx

include Makefile.local

# Drasi
CFLAGS:=-I. $(shell $(DRASI_DIR)/bin/drasi-config.sh --cflags) -ggdb3 -O3
LIBS:=$(shell $(DRASI_DIR)/bin/drasi-config.sh --libs) -O3

# ALPIDE
CFLAGS+=-I$(ALPIDE_DIR)/framework/src/mosaic \
        -I$(ALPIDE_DIR)/framework/src/manager \
        -I$(ALPIDE_DIR)/framework/src/common \

LIBS:=$(LIBS) \
	$(ALPIDE_DIR)/framework/lib/libMANAGER.a \
	$(ALPIDE_DIR)/framework/lib/libMOSAIC.a \
	$(ALPIDE_DIR)/framework/lib/libCOMMON.a -lusb-1.0 \
	$(shell root-config --libs)

MKDIR=[ -d $(@D) ] || mkdir -p $(@D)


all: $(TARGET)

$(TARGET): $(OBJ)
	$(MKDIR)
	g++ -o $@ $^ $(LIBS)

$(BUILD_DIR)/%.o: %.c Makefile
	$(MKDIR)
	g++ -c -o $@ $< $(CFLAGS)

# ---------------------------- #
scan: $(SCAN_TARGET)

$(SCAN_TARGET) : $(SCAN_OBJ)
	$(MKDIR)
	g++ -m64 $< -o $@ $(LIBS)

$(SCAN_OBJ) : $(SCAN_SRC)
	$(MKDIR)
	g++ -std=c++17 -c -g $(CFLAGS) $< -o $@


.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) *~
