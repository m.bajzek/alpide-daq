#!/bin/bash

source alpide.conf

for l in $ALPIDE_ARRAY
do
    name=$(printf "run%d" $l)
    screen -S alpide -p $name -X stuff "
./run.bash $l to
"
done
