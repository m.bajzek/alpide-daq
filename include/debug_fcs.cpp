void print_buffer(void* buffer, int nBytes) {
    unsigned* p32 = (unsigned*)buffer;

    for(int i=0; nBytes > 0; ++i) {
        printf("%08x ", *p32);
        if(i%8 == 7) printf("\n");
        nBytes -= 4;
    }
} 
