// List of handy stuff --lrose
#include <stdio.h>
#include <stdlib.h>

#define BARW (sizeof BAR - 1 )
#define BAR ">>>>>>>>>>>>>>>>>>>>>>>>"
void printProgress(double percentage)
{
	int val = (int)(percentage*100);
	int lpad = (int)(percentage*BARW);
	int rpad = BARW - lpad;
	printf("\r%3d%% [%.*s%*s]",val,lpad,BAR,rpad,"");
	fflush(stdout);
}
