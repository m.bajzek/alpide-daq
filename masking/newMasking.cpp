#include "TSetup.h"
#include "TDevice.h"
#include "TScanConfig.h"
#include "TDeviceOccupancyScan.h"
#include "TDeviceBuilderIBSingleMosaic.h"
#include "TReadoutBoard.h"
#include "TReadoutBoardMOSAIC.h"
#include "TBoardConfigMOSAIC.h"
#include <bits/stdc++.h>
#include <chrono>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using namespace std;

#define ROW_MIN 0
#define ROW_MAX 512
#define COL_MIN 0
#define COL_MAX 1024

#define timeNow() std::chrono::high_resolution_clock::now()



/*
 *  Mask can be applied to:
 *  --> Individual pixels: COL,ROW       (e.g. 201,405)
 *  --> Single Row:        rROW          (e.g. r213)
 *  --> Multiple Rows:     rROW1...ROW2  (e.g. r213...250), both inclusive
 *  --> Single Column:     cCOL          (e.g. c777)
 *  --> Multiple Columns:  cCOL1...COL2  (e.g. c777...888), both inclusive
 *  --> Rectangular area: state the upper-left corner (COL1,ROW1) and bottom-right
 *      corner (COL2,ROW2) in the following manner: (COL1,ROW1)...(COL2,ROW2)
 *                                       (e.g. 100,101...102,103)
 *  It is also possible to pass a callback function handle 
 *  --> bool foo(int x, int y) which returns true for pixels to be masked.
 */

void Trim(string& s) {
    /* std::remove_if() kills the elements satisfying the lambda then returns
     * an iterator to the end of the 'new' string. Then we just need to erase all from
     * that point to the end of original s. */ 
    s.erase(std::remove_if(s.begin(), s.end(), [](unsigned char c){return std::isspace(c);}), s.end());
}

void ApplyMask(TAlpide* alpide, const char* fileName, const function<bool(int,int)>& func = nullptr) {
    if(!alpide) return;
    auto t1 = timeNow();
    cout << "Applying mask ...\n" << dec;
    /* If a function handle is provided: */
    if(func) {
        for(int x=ROW_MIN; x<ROW_MAX; ++x) {
            for(int y=COL_MIN; y<COL_MAX; ++y)
                if(func(x,y)) alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, y, x);
        }
    } //FIXME chamge x and y in func handle 
    /* Reading the mask config file */
    ifstream file(fileName);
    if(!file) return;
    string line;
    smatch m;
    while(getline(file, line)) {
        Trim(line);
        if(line.size == 0 || line[0]=='#') continue;

        //Function For Check Bored --Lrose
        int check = (int)line.find("check");
        //if(check != -1) cout <<"Checker Board Pattern found at index " << check << endl; 
        //else cout <<"Checker Board Pattern not found yet ... " << check << endl; 
        //cout << line << "  " << __LINE__ << endl;
        if(check != -1 ){
            cout << "WARN: Takes some time."<<endl;          
            for(int rowTemp=0; rowTemp<ROW_MAX/2; rowTemp++){
                double PercentageDone = 2*(double)(rowTemp+1.)/ROW_MAX;
                printProgress(PercentageDone);
                for(int colTemp=0; colTemp<COL_MAX; colTemp++){
                    alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, 2*rowTemp+1, 2*colTemp+1);
                }
            }
        }
        // End of Luke checkerboard
        //
        int a,b;
        if(regex_match(line, m, regex("([0-9]+)\\,([0-9]+)"))) { // one pixel
            try {
                a = stoi(m[1]); b = stoi(m[2]);
                if(a<COL_MIN || a>=COL_MAX || b<ROW_MIN || b>=ROW_MAX) throw domain_error("Bounds violation"); 
                alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, b, a);
            }
            catch(const exception& e) {continue;}
        }
        else if(regex_match(line, m, regex("(c|r)([0-9]+)"))) { // one col/row
            try {
                a = stoi(m[2]);
                if(line[0]=='c' && (a<COL_MIN || a>=COL_MAX)) throw domain_error("Bounds violation");
                if(line[0]=='r' && (a<ROW_MIN || a>=ROW_MAX)) throw domain_error("Bounds violation");
                
                if(line[0]=='c') {
                    for(int rowID=ROW_MIN; rowID<ROW_MAX; ++rowID)
                        alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, a); 
                }
                else {
                    for(int colID=COL_MIN; rowID<COL_MAX; ++rowID)
                        alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, a, colID); 
                }
            }
            catch(const exception& e) {continue;}
        }
        else if(regex_match(line, m, regex("(c|r)([0-9]+)\\.\\.\\.([0-9]+)"))) { //multiple cols/rows
            try {
                a = stoi(m[2]); b = stoi(m[3]);
                if(line[0]=='c' && (a<COL_MIN || a>=COL_MAX || b<COL_MIN || b>=COL_MAX)) throw domain_error("Bounds violation");    
                if(line[0]=='r' && (a<ROW_MIN || a>=ROW_MAX || b<ROW_MIN || b>=ROW_MAX)) throw domain_error("Bounds violation");    
                if(line[0]=='c') {
                    for(int rowID=ROW_MIN; rowID<ROW_MAX; ++rowID){
                        for(int colID=a; colID<=b; ++colID)
                            alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID);
                    }
                }
                else {
                    for(int rowID=a; rowID<=b; ++rowID){
                        for(int colID=COL_MIN; colID<COL_MAX; ++colID)
                            alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID);
                    }
                }
            }
            catch(const exception& e) {continue;}           
        }
        else if(regex_match(line,m, regex("([0-9]+)\\,([0-9]+)\\.\\.\\.([0-9]+)\\,([0-9]+)"))) { //rectangle 
            try{
                a  = stoi(m[1]); b  = stoi(m[2]);
                a1 = stoi(m[3]); b1 = stoi(m[4]);
                if(a<COL_MIN || b<ROW_MIN || a>=COL_MAX || b>=ROW_MAX) throw domain_error("Bounds violation");
                if(a1<COL_MIN || b1<ROW_MIN || a1>=COL_MAX || b1>=ROW_MAX) throw domain_error("Bounds violation");
                for(int colID=a; colID<=a1; ++colID){
                    for(int rowID=b; rowID<=b1; ++rowID)
                        alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID);
                }
            }
            catch(const exception& e){continue;}
        }
    } // end of reading file 
    
    auto t2 = timeNow();
    cout << "Done with masking ... \n";
    cout << "Time taken: " << duration_cast<milliseconds>(t2-t1).count() << " ms" << endl;
}
