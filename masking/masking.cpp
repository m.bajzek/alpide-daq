#include "TSetup.h"
#include "TDevice.h"
#include "TScanConfig.h"
#include "TDeviceOccupancyScan.h"
#include "TDeviceBuilderIBSingleMosaic.h"
#include "TReadoutBoard.h"
#include "TReadoutBoardMOSAIC.h"
#include "TBoardConfigMOSAIC.h"
#include <bits/stdc++.h>
#include <chrono>
#include "../include/functions.h"
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using namespace std;

#define ROW_MIN 0
#define ROW_MAX 512
#define COL_MIN 0
#define COL_MAX 1024

#define timeNow() std::chrono::high_resolution_clock::now()



/*
 *  Mask can be applied to:
 *  --> Individual pixels: COL,ROW       (e.g. 201,405)
 *  --> Single Row:        rROW          (e.g. r213)
 *  --> Multiple Rows:     rROW1...ROW2  (e.g. r213...250), both inclusive
 *  --> Single Column:     cCOL          (e.g. c777)
 *  --> Multiple Columns:  cCOL1...COL2  (e.g. c777...888), both inclusive
 *  --> Rectangular area: state the upper-left corner (COL1,ROW1) and bottom-right
 *      corner (COL2,ROW2) in the following manner: (COL1,ROW1)...(COL2,ROW2)
 *                                       (e.g. 100,101...102,103)
 *  It is also possible to pass a callback function handle 
 *  --> bool foo(int x, int y) which returns true for pixels to be masked.
 */
#if 0
#define MASKING_DEBUG
#endif
void Trim(string& s) {
    /* std::remove_if() kills the elements satisfying the lambda then returns
     * an iterator the end of the 'new' string. Then we just need to erase all from
     * that point to the end of original s. */ 
    s.erase(std::remove_if(s.begin(), s.end(), [](unsigned char c){return std::isspace(c);}), s.end());
}

void ApplyMask(TAlpide* alpide, const char* fileName, const function<bool(int,int)>& func = nullptr) {
    if(!alpide) return;
    auto t1 = timeNow();
    cout << "Applying mask ...\n" << dec;
    /* If a function handle is provided: */
    if(func) {
        for(int x=ROW_MIN; x<ROW_MAX; ++x) {
            for(int y=COL_MIN; y<COL_MAX; ++y)
                if(func(x,y)) alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, y, x);
        }
    }
    /* Reading the mask config file */
    ifstream file(fileName);
    if(!file) return;
    string line;
    while(getline(file, line)) {
        Trim(line);
        int N=line.size(); if(N==0 || line[0]=='#') continue;
        size_t stoiSize{};
        
        //Function For Check Bored --Lrose
        int check = (int)line.find("check");
        //if(check != -1) cout <<"Checker Board Pattern found at index " << check << endl; 
        //else cout <<"Checker Board Pattern not found yet ... " << check << endl; 
        //cout << line << "  " << __LINE__ << endl;
        if(check != -1 ){
            cout << "WARN: Takes some time."<<endl;          
            for(int rowTemp=0; rowTemp<ROW_MAX/2; rowTemp++){
                double PercentageDone = 2*(double)(rowTemp+1.)/ROW_MAX;
                
                for(int colTemp=0; colTemp<COL_MAX; colTemp++){
                    alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, 2*rowTemp+1, 2*colTemp+1);
                }
            }
        }
        // End of Luke checkerboard

        /* TODO : rewrite all the stoi checks with simple regex, check
         * newMasking.cpp */
        
        if(line[0] == 'c' || line[0] == 'r') {
            int threeDotsPosition = (int)line.find("...");
            int a,b;
            if(threeDotsPosition == -1) { // Mask one col/row
                try {
                    a = stoi(line.substr(1,line.size()), &stoiSize);
                    if(stoiSize != N-1) throw runtime_error("Unparsable chars in stoi()");
                    if(line[0]=='c' && (a<COL_MIN || a>=COL_MAX)) throw domain_error("Bounds violation");
                    if(line[0]=='r' && (a<ROW_MIN || a>=ROW_MAX)) throw domain_error("Bounds violation");
                    b = a;
                }
                catch(const exception& e) {continue;}
            }
            else { // Mask multiple row/cols
                try {
                    a = stoi(line.substr(1, threeDotsPosition-1), &stoiSize);
                    if(stoiSize != threeDotsPosition-1) throw runtime_error("Unparsable chars after stoi() before \"...\"");
                    b = stoi(line.substr(threeDotsPosition+3, line.size()), &stoiSize);
                    if(stoiSize != N-3-threeDotsPosition) throw runtime_error("Unparsable chars after stoi()");

                    if(a < 0 || b < 0) throw domain_error("Bounds violation");
                    if(line[0] == 'c' && (a < COL_MIN || b < COL_MIN || a>=COL_MAX || b>=COL_MAX)) throw domain_error("Bounds violation");
                    if(line[0] == 'r' && (a < ROW_MIN || b < ROW_MIN || a>=ROW_MAX || b>=ROW_MAX)) throw domain_error("Bounds violation");
                }
                catch(const exception& e) {continue;}
            }
            if(line[0] == 'r')  {
                for(int rowID=a; rowID<=b; ++rowID){
                    for(int colID=COL_MIN; colID<COL_MAX; ++colID)
                        alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID+1);
                }
            }
            else { // line[0] == 'c'
                for(int rowID=ROW_MIN; rowID<ROW_MAX; ++rowID){
                    for(int colID=a; colID<=b; ++colID)
                        alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID+1);
                } 
            }
        }

        /* Mask individual pixels or a rectangle */
        else {
            int commaPosition = (int)line.find(',');
            if(commaPosition == -1) continue;

            int threeDotsPosition = (int)line.find("...");
            if(threeDotsPosition == -1) { // Mask one pixel
                try {
                    int a = stoi(line.substr(0, commaPosition), &stoiSize);
                    if(stoiSize != commaPosition) throw runtime_error("Unparsable chars after stoi() before ','"); 
                    int b = stoi(line.substr(commaPosition+1, line.size()), &stoiSize);
                    if(stoiSize != N-1-commaPosition) throw runtime_error("Unparsable chars after 2nd stoi()");
                    if(a<COL_MIN || b<ROW_MIN || a>=COL_MAX || b>=ROW_MAX) throw domain_error("Bounds violation");
                    alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, b, a);
#ifdef MASKING_DEBUG
                    cout << "<< Masked (" << a << "," << b << ")\n";
#endif
                }
                catch(const exception& e) {continue;}
            }
            else { // Mask a rectangle
                try { // Format: 201,304...203,305
                    int secondCommaPosition = (int)line.find(',', commaPosition+1);
                    if(secondCommaPosition == -1) throw runtime_error("No second pair of ints");
                    int a1 = stoi(line.substr(0, commaPosition), &stoiSize);
                    if(stoiSize != commaPosition) throw runtime_error("Unparsable chars before first ','");
                    int b1 = stoi(line.substr(commaPosition+1, threeDotsPosition-commaPosition-1), &stoiSize);
                    if(stoiSize != threeDotsPosition-commaPosition-1) throw runtime_error("Unparsable chars before \"...\"");
                    int a2 = stoi(line.substr(threeDotsPosition+3, secondCommaPosition-threeDotsPosition-3), &stoiSize);
                    if(stoiSize != secondCommaPosition-threeDotsPosition-3) throw runtime_error("Unparsable chars before 2nd ','");
                    int b2 = stoi(line.substr(secondCommaPosition+1, line.size()), &stoiSize);
                    if(stoiSize != N-1-secondCommaPosition) throw runtime_error("Unparsable chars after 2nd ','");

                    if(a1<COL_MIN || b1<ROW_MIN || a1>=COL_MAX || b1>=ROW_MAX) throw domain_error("Bounds violation");
                    if(a2<COL_MIN || b2<ROW_MIN || a2>=COL_MAX || b2>=ROW_MAX) throw domain_error("Bounds violation");
                    cout << ">> Read (" << a1 << "," << b1 << ")"<< ",(" << a2 << "," << b2  <<")\n";
                    for(int colID=a1; colID<=a2; ++colID){
                        for(int rowID=b1; rowID<=b2; ++rowID) {
                            alpide->WritePixRegSingle(AlpidePixConfigReg::MASK_ENABLE, true, rowID, colID);
#ifdef MASKING_DEBUG
                            cout << "<< Masked (" << colID+1 << "," << rowID << ")\n";
#endif
                        }
                    }
                }
                catch(const exception& e) {continue;}
            }
        }    
    } // end of reading file 
    auto t2 = timeNow();
    cout << "Done with masking ... \n";
    cout << "Time taken: " << duration_cast<milliseconds>(t2-t1).count() << " ms" << endl;
}
